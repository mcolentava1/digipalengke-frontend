//var $globalServer = "https://digipalengke.df.r.appspot.com";
//var $globalServer = "http://localhost/public/digipalengke/api";
var $globalServer = "https://digipalengke.herokuapp.com/api/v1";



//********* DEFAULT VALUES *************//

//Shop Default Value
const product = [
    {name: "Rice", price: 45.00, unit: "kg", max: "10", stock: "100", increment: "0.5"},
    {name: "Slice Bread", price: 50.00, unit: "loaf", max: "10", stock: "100", increment: "1"},
    {name: "Egg", price: 10.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Butter", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Cheese", price: 40.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    
    {name: "Chicken", price: 150.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Pork", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Beef", price: 300.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Tilapia", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Bangus", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    
    {name: "Galunggong", price: 180.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Tinapa", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Hipon", price: 600.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Pusit", price: 500.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    {name: "Saging/Banana", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.25"},
    
    {name: "Pongkan", price: 30.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Apple", price: 30.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Bawang", price: 30.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Sibuyas", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Luya", price: 50.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    
    {name: "Kamatis", price: 300.00, unit: "kg", max: "10", stock: "100", increment: "0.1"},
    {name: "Kalamansi", price: 200.00, unit: "kg", max: "10", stock: "100", increment: "0.1"},
    {name: "Pechay", price: 45.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Repolyo", price: 45.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Carrots", price: 45.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    
    {name: "Toyo", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Suka", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Patis", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Ketchup", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Asin", price: 10.00, unit: "bag", max: "10", stock: "100", increment: "1"},
    
    {name: "Asukal", price: 15.00, unit: "kg", max: "10", stock: "100", increment: "1"},
    {name: "Mantika", price: 20.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Paminta", price: 2.00, unit: "pack", max: "10", stock: "100", increment: "1"},
    {name: "Sardinas", price: 30.00, unit: "can", max: "10", stock: "100", increment: "1"},
    {name: "Tuna", price: 45.00, unit: "can", max: "10", stock: "100", increment: "1"},
    
    {name: "Pancit Canton", price: 10.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Instant Mami", price: 10.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Instant Coffee", price: 5.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Powdered Milk", price: 100.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Formula Milk 1", price: 500.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    
    {name: "Formula Milk 2", price: 400.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Formula Milk 3", price: 300.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Diaper 1", price: 200.00, unit: "pack", max: "10", stock: "100", increment: "1"},
    {name: "Diaper 2", price: 400.00, unit: "pack", max: "10", stock: "100", increment: "1"},
    {name: "Bleach", price: 200.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    
    {name: "Laundry Detergent", price: 21.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Fabcon", price: 22.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Dishwashing Soap", price: 22.00, unit: "pc", max: "10", stock: "100", increment: "1"},
    {name: "Tissue Paper", price: 15.00, unit: "roll", max: "10", stock: "100", increment: "1"},
    {name: "PPE Donation", price: 20.00, unit: "+", max: "100", stock: "100", increment: "1"}
];
const productCount = product.length;

//Locations
const brgyCity = [
    {city: "Balanga City", coveredBarangays:["Munting Batangas","Tenejero","Camacho","Bagong Silang","Bagumbayan"]},
    {city: "Dinalupihan", coveredBarangays:["Olongapo","Bagong Pari","Danana",]}
    ];

//********* END: DEFAULT VALUES *************//

function addComma(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
function limitShop(){
    
    var HttpURL = $globalServer + "/countOrder";
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();
                
        var data = JSON.parse(request.responseText);
        var $response = data.response;
        var $orderCount = data.orderCount;
            $orderCount = parseInt($orderCount);
    
        //Note: this is a static limit of 30 transactions
        if($orderCount > 30){
            alert("ORDERING CLOSED: Sorry, dahil sa mataas na bilang ng order, na-reach na ng DigiPalengke ang daily order limit nito para sa susunod na delivery. Kami ay muling tatanggap ng order sa ganap na 3:01PM.");
            localStorage.setItem('TotalPrice', 0);
            window.location.replace("index.html?ref=daily+order+limit");
        }else{
            //console.log("Count still good at " + $orderCount);
        }
    
    
    
}
function loadbarangay(){
    var selectedCity = localStorage.getItem('selectedCity'); 
        for(n=0; n < brgyCity.length; n++){
            
            var arrayCity = brgyCity[n]['city'];
            
            if(arrayCity == selectedCity){
                var coveredBarangay = brgyCity[n]['coveredBarangays'];

                var optionBuild = "<option disabled selected>Select Barangay</option>";
                for(n=0; n < coveredBarangay.length; n++){
                    optionBuild += "<option>"+coveredBarangay[n]+"</option>";
                }
                document.getElementById('barangay').innerHTML = optionBuild;

            }
        }
}

//Security
function priceEncrypt(price, totalQty){
    
    var code = price + totalQty + 12345620932397890;
    var hash = md5(code);
    localStorage.setItem('priceCrypt', hash);
}
function priceDecrypt(){
    var price = localStorage.getItem('TotalPrice'); 
        price = parseFloat(price);
    var totalQty = localStorage.getItem('totalQty'); 
        totalQty = parseFloat(totalQty);
    var expectedValue = localStorage.getItem('priceCrypt'); 
    
    var code = price + totalQty + 12345620932397890;
    var hash = md5(code);
    
    console.log(hash + " | " + expectedValue);
    
    if(hash != expectedValue){
        
        
        alert('We have detected an unusual activity from your device. We are will collect necessary information from your transaction for further investigation.');
        //window.location.replace("https://www.lawphil.net/statutes/repacts/ra2012/ra_10175_2012.html");
    }else{
        console.log('Price Match');
    }
    
}

function apiKeygen(keywords){
    var salt = "SXsfyOwfA1";
    var key =  keywords + salt;
    var returnVal = md5(key);
    
    return returnVal;
}

function visitFacebook(){
    window.location.replace("https://fb.com/digipalengke");
}

function sendFeedback(){
    window.location.replace("https://fb.com/digipalengke");
}