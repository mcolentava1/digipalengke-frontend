window.addEventListener("load", loadlogin);

localStorage.clear();
/**** DEFAULT VALUE ******/
var $storename = "Demo Store";
var $storekey = "$2a$14$RXz5i78BD6yWYcvGp36Bs.sZtl2RSJRo1JTpadxJcDaEA5w2MVvbW";
var $storecity = "Balanga City";

localStorage.setItem('storename', $storename);
localStorage.setItem('storekey', $storekey);
localStorage.setItem('selectedCity', $storecity);


function loadlogin(){
    var selectedStore = localStorage.getItem('storename');
    document.getElementById('storename').innerText = selectedStore;
}

 window.fbAsyncInit = function() {
    FB.init({
      appId      : '170355390741820',
      cookie     : true,
      xfbml      : true,
      version    : 'v7.0'
    });
      
//      FB.AppEvents.logPageView();   
      
      
        function statusChangeCallback(response) { 
                if (response.status === 'connected') {      
                    sessionStorage.setItem('fb_id', response.name );
                    sessionStorage.setItem('fb_email', response.email );   
                    window.location.replace("2order.html");
                }
            } 
      
      
  }; //end of fb async
    

 function loginOnFacebook(){
           FB.login(function(response) {
                if(response.authResponse){
                 FB.api('/me', function(response) {    
                    sessionStorage.setItem('fb_id', response.name );    
                    sessionStorage.setItem('fb_email', response.email );   
                        window.location.replace("2order.html");                     
                 });
                    
                }else{
                 console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'public_profile,email'});  
          
 
      }

      (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));