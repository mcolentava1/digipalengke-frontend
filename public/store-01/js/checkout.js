
window.addEventListener("load", loadCheckout);
function loadCheckout(){
    
    
    /* Store Limit Stop */
    limitShop();
    setInterval(limitShop, 120000);

    localStorage.setItem('deliveryFee', 0);
    localStorage.setItem('paymentFee', 0);
    localStorage.setItem('paymentfeepercent', 1);
    localStorage.setItem('paymentfeefixed', 0);

    loadbarangay();
    calculateTotal();

}


//Form Listeners
{
document.getElementById("fullname").addEventListener("change", validateFullname);
document.getElementById("mobileno").addEventListener("change", validationMobile);
document.getElementById("email").addEventListener("change", validationEmail);
document.getElementById("houseno").addEventListener("click", validationHouseno);
document.getElementById("street").addEventListener("change", validationStreet);
document.getElementById("barangay").addEventListener("change", validationBrgy);
document.getElementById("paymentOption1").addEventListener("click", function(){ setPayment(1); });
document.getElementById("paymentOption2").addEventListener("click", function(){ setPayment(2); });
document.getElementById("deliveryOption1").addEventListener("click", function(){ setDelivery(1); });
document.getElementById("deliveryOption2").addEventListener("click", function(){ setDelivery(2); });

document.getElementById("sendInfo").addEventListener("click", orderOTP);
document.getElementById("confirmOTP").addEventListener("click", orderComplete);
    
}

// ------- Form Process -------//

function validateFullname(){
    var value = document.getElementById('fullname').value;
        if(value.length < 7){
            document.getElementById('fullname').style.borderColor = "#b33939";
        }else{
            document.getElementById('fullname').style.borderColor = "#33d9b2";
        }   
}
function validationMobile(){
    var value = document.getElementById('mobileno').value;
        if(value.length != 11){
            document.getElementById('mobileno').style.borderColor = "#b33939";
        }else{
            document.getElementById('mobileno').style.borderColor = "#33d9b2";
            document.getElementById('otpmobile').innerText = value;
            
        }   
}
function validationEmail(){
    var value = document.getElementById('email').value;
    
     if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/.test(value))){
            document.getElementById('email').style.borderColor = "#b33939";
        }else{
            document.getElementById('email').style.borderColor = "#33d9b2";
        }   
}
function validationHouseno(){
        //click is fine
        document.getElementById('houseno').style.borderColor = "#33d9b2";
  
}
function validationStreet(){
    var value = document.getElementById('street').value;
        if(value.length < 3){
            document.getElementById('street').style.borderColor = "#b33939";
        }else{
            document.getElementById('street').style.borderColor = "#33d9b2";
        }   
}
function validationBrgy(){
    var value = document.getElementById('barangay').value;
        if(value.length < 1){
            document.getElementById('barangay').style.borderColor = "#b33939";
        }else{
            document.getElementById('barangay').style.borderColor = "#33d9b2";
        }   
}
function setPayment(option){
    switch(option){
        case 1:
            var optionValue = "cod";
            var paymentOption1 = "#218c74";
            var paymentOption2 = "#cfd5e3";
            var paymentOption3 = "#cfd5e3";
            var paymentfeepercent = 1;
            var paymentfeefixed = 0;
        break;
        case 2:
            var optionValue = "creditcard";
            var paymentOption1 = "#cfd5e3";
            var paymentOption2 = "#218c74";
            var paymentOption3 = "#cfd5e3";
            var paymentfeepercent = 1.015;
            var paymentfeefixed = 0;
        break;
        case 3:
            var optionValue = "gcash";
            var paymentOption1 = "#cfd5e3";
            var paymentOption2 = "#cfd5e3";
            var paymentOption3 = "#218c74";
            var paymentfeepercent = 1;
            var paymentfeefixed = 0;
        break;
    }
    
    document.getElementById("paymentMethod").value = optionValue;
    document.getElementById("paymentOption1").style.borderColor = paymentOption1;
    document.getElementById("paymentOption2").style.borderColor = paymentOption2;
    document.getElementById("paymentOption3").style.borderColor = paymentOption3;
    localStorage.setItem('paymentfeepercent', paymentfeepercent);
    localStorage.setItem('paymentfeefixed', paymentfeefixed);
    calculateTotal();
}
function setDelivery(option){
    switch(option){
        case 1:
            var optionValue = "pickup";
            var deliveryOption1 = "#218c74";
            var deliveryOption2 = "#cfd5e3";
            var deliveryFee = 0;
        break;
        case 2:
            var optionValue = "delivery";
            var deliveryOption1 = "#cfd5e3";
            var deliveryOption2 = "#218c74";
            var deliveryFee = 30;
        break;
    }
    
    document.getElementById("deliverymethod").value = optionValue;
    document.getElementById("deliveryOption1").style.borderColor = deliveryOption1;
    document.getElementById("deliveryOption2").style.borderColor = deliveryOption2;
    localStorage.setItem('deliveryFee', deliveryFee);
    calculateTotal();
}
function calculateTotal(){
    
    var totalPrice = localStorage.getItem('TotalPrice'); totalPrice = parseFloat(totalPrice);
    var deliveryFee = localStorage.getItem('deliveryFee');  deliveryFee = parseFloat(deliveryFee);
    
    var paymentfeepercent = localStorage.getItem('paymentfeepercent');  paymentfeepercent = parseFloat(paymentfeepercent);
    
    var paymentfeefixed = localStorage.getItem('paymentfeefixed');  paymentfeefixed = parseFloat(paymentfeefixed);
    
        //security check
        if(deliveryFee < 0 || paymentfeefixed < 0){
            alert('We have detected an unusual activity from your device. We are will collect necessary information from your transaction for further investigation.');
            window.location.replace("https://www.lawphil.net/statutes/repacts/ra2012/ra_10175_2012.html");
            throw new Error("Unknown server error.");
        }
        
    var finalPrice = ((totalPrice + deliveryFee) * paymentfeepercent) + paymentfeefixed;
    var paymentFee = ((totalPrice + deliveryFee) * (paymentfeepercent - 1)) + paymentfeefixed;
    
    localStorage.setItem('finalPaymentFee', paymentFee);
    localStorage.setItem('finalPrice', finalPrice);
    document.getElementById('subtotal').innerText = addComma(finalPrice.toFixed(2));
    
    
}

// ---- Core Process ---- //

function orderOTP(){
    document.getElementById('sendInfo').innerHTML = "<img src='../img/loading.svg' height='18px'>";
    setTimeout(function(){ 
        
    var error = 0;
    var errorMessage = "";
           
    var fullname = document.getElementById('fullname').value;
    var mobileno = document.getElementById('mobileno').value;
    var emailadd = document.getElementById('email').value;
    var houseno = document.getElementById('houseno').value;
    var street = document.getElementById('street').value;
    var barangay = document.getElementById('barangay').value;
    var landmark = document.getElementById('landmark').value;
    var city = document.getElementById('city').value;
    var paymentMethod = document.getElementById('paymentMethod').value;
    var deliverymethod = document.getElementById('deliverymethod').value;
    var termsandconditions1 = document.getElementById('termsandconditions1').checked;
    var termsandconditions2 = document.getElementById('termsandconditions2').checked;
        
    //PERSONAL INFO CHECK
    if(termsandconditions2 != true){ error = 1; errorMessage = "Basahin at sumang-ayon sa ating TERMS and CONDITIONS."; }
    if(termsandconditions1 != true){ error = 1; errorMessage = "Basahin at sumang-ayon sa ating TERMS and CONDITIONS."; }
    if(deliverymethod.length < 2){ error = 1; errorMessage = "Oops! Kailangang pumili ng DELIVERY Method."; }
    if(paymentMethod.length < 2){ error = 1; errorMessage = "Oops! Kailangang pumili ng PAYMENT Method."; }
    if(city.length < 2){ error = 1; errorMessage = "Oops! Kailangang pumili ng PAYMENT Method."; }
    if(barangay.length < 2){ error = 1; errorMessage = "Oops! Kailangan ang detalye ng BARANGAY."; }
    if(street.length < 3){ error = 1; errorMessage = "Oops! Kailangan ang detalye ng STREET/SUBDIVISION/PUROK."; }
    if(mobileno.length != 11){ error = 1; errorMessage = "Oops! Hindi valid ang iyong MOBILE NUMBER"; }
    if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/.test(emailadd))){
        error = 1; errorMessage = "Opps! Hindi valid ang EMAIL na iyong ibinigay.";
    }
    if(fullname.length < 7){ error = 1; errorMessage = "Oops! Kailangan ang iyong buong PANGALAN."; }
        
    if(error > 0){
        
            document.getElementById('formErrMsg').innerHTML = errorMessage;
            document.getElementById('formErrMsg').style.display = 'block';
            document.getElementById('sendInfo').innerHTML = "Try Again! <span class='material-icons'>arrow_right_alt</span>";

    }else{

            document.getElementById('formErrMsg').style.display = 'none';
    
        
            if(paymentMethod == 'creditcard'){
                orderComplete(1);
            }else{
                    //OTP Server Call
                    var key = apiKeygen(mobileno);
                    var HttpURL = $globalServer + "/otpgenerate?key=" + key + "&mobile=" + mobileno;
                    request = new XMLHttpRequest();
                    request.open("GET", HttpURL ,false);
                    request.send();
                

                    var data = JSON.parse(request.responseText);
                    var $response = data.response;
                    var $message = data.message;

            
                    if($response == 'success'){
                        document.getElementById('sendInfo').style.display = "none";
                        document.getElementById('otpbox').style.display = "block";
                        
                    }else{
                       document.getElementById('formErrMsg').innerHTML = $message;
                        document.getElementById('formErrMsg').style.display = 'block';
                        document.getElementById('sendInfo').innerHTML = "Try Again! <span class='material-icons'>arrow_right_alt</span>";
                    } 
            }         
                       
        
    }
        
    
    }, 2000);

}
function orderComplete(bypass){
    
                //SECURITY CHECK
                priceDecrypt();
    
                if(bypass != 1){
                   var otpNumber = document.getElementById('otpInputForm').value;
                    var mobileNo = document.getElementById('mobileno').value;
                    var key = apiKeygen(mobileNo);

                    var HttpURL = $globalServer + "/otpvalidate?key=" + key + "&mobile=" + mobileNo + "&otp=" + otpNumber;

                    request = new XMLHttpRequest();
                    request.open("GET", HttpURL ,false);
                    request.send();

                    var data = JSON.parse(request.responseText);
                    var $response = data.response;
                    var $message = data.message; 
                }else{
                    var $response = 'success';
                }
                
    
    
    
            
                if($response == 'success'){
                    
                    //Recalculate total
                    calculateTotal();
                    
                    //Get value of form
                    var fullname = document.getElementById('fullname').value;
                    var mobileno = document.getElementById('mobileno').value;
                    var emailadd = document.getElementById('email').value;
                    var houseno = document.getElementById('houseno').value;
                    var street = document.getElementById('street').value;
                    var barangay = document.getElementById('barangay').value;
                    var landmark = document.getElementById('landmark').value;
                    var city = document.getElementById('city').value;
                    var paymentMethod = document.getElementById('paymentMethod').value;
                    var deliverymethod = document.getElementById('deliverymethod').value;
                    
                    
                    //Generate Product Array
                    var totalPrice = 0;
                    var totalProduct = 0;
                    var totalQty = 0;
                    var orderSummaryi = 0;
                    var orderSummary = [];
                        for(i=0 ; i < productCount || i>3000; i++){
                                var variableNo = i + 1;

                                var storageName = "ORDER_qty" + variableNo;
                                var productQty = localStorage.getItem(storageName); productQty = parseFloat(productQty);
                                var storageName = "ORDER_id" + variableNo;
                                var prodID = localStorage.getItem(storageName); prodID = parseInt(prodID);
                                var storageName = "ORDER_price" + variableNo;
                                var productPrice = localStorage.getItem(storageName); productPrice = parseFloat(productPrice);
                                var storageName = "ORDER_name" + variableNo;
                                var productNameStored = localStorage.getItem(storageName);
                                
                                totalPrice = totalPrice + productPrice;
                                totalQty = totalQty + productQty;
                                if(productQty > 0){totalProduct = totalProduct + 1;}
                        
                                //generate order summary
                                if(productQty > 0){
                                    
                                    orderSummary[orderSummaryi] = {
                                    productID: prodID,
                                    productName: productNameStored,
                                    productQty: productQty,
                                    ProductPrice: productPrice
                                    };
                                    orderSummaryi = orderSummaryi + 1;
                                }
                        
                          }
                    
                    //Price commputation
                    var deliveryfee = localStorage.getItem('deliveryFee'); deliveryfee = parseFloat(deliveryfee);
                    var paymentFee = localStorage.getItem('finalPaymentFee'); paymentFee = parseFloat(paymentFee);
                    var otherFees = localStorage.getItem('otherFees'); otherFees = parseFloat(otherFees);
                    var subtotal = localStorage.getItem('TotalPrice'); subtotal = parseFloat(subtotal);
                    var finalPrice = localStorage.getItem('finalPrice'); finalPrice = parseFloat(finalPrice);
                    
                    //Login Credentials
                    var fb_email = sessionStorage.getItem('fb_email');
                    var fb_id = sessionStorage.getItem('fb_id');
                    var $storename = localStorage.getItem('storename');
                    var $storekey = localStorage.getItem('storekey');
                    
                    
                    
                    //Generate JSON Order List
                    var orderList = JSON.stringify(orderSummary);
                    //Security
                    var apisendkey = finalPrice + "" + mobileno + "" + fb_id;
                        apisendkey = apiKeygen(apisendkey);
     
                            var status = "unpaid";
                            var HttpURL = $globalServer + "/donesales";
                            var dataURL = "key="+ encodeURI(apisendkey) + "&storekey="+ encodeURI($storekey) + "&storename=" + encodeURI($storename) + "&fb_email="+ encodeURI(emailadd) + "&fb_id="+encodeURI(fb_id)+ "&fullname="+encodeURI(fullname)+ "&mobile="+encodeURI(mobileno)+ "&unitno="+encodeURI(houseno)+ "&street="+encodeURI(street)+ "&barangay="+encodeURI(barangay)+ "&landmark="+encodeURI(landmark)+ "&city="+encodeURI(city)+ "&paymentmode="+encodeURI(paymentMethod)+ "&deliverymode="+encodeURI(deliverymethod)+ "&totalprice="+finalPrice+ "&subtotal="+encodeURI(subtotal)+ "&deliveryfee="+encodeURI(deliveryfee)+"&otherfees="+encodeURI(otherFees)+"&paymentfee="+encodeURI(paymentFee)+"&status="+encodeURI(status)+"&orderlist="+encodeURI(orderList)+"";
        
                            request = new XMLHttpRequest();
                            request.open("POST", HttpURL ,false);
                            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            request.send(dataURL);

                            var data = JSON.parse(request.responseText);
                            var $response = data.response;
                            var $message = data.message;
                            var $orderNo = data.orderno;
                            
                                console.log(data);
                    
                            if($response == 'success'){
                                
                                //Loading screen
                                document.getElementById('confirmOTP').innerHTML = "<img src='../img/loading.svg' height='18px'>";
    
                                
                                switch(paymentMethod)
                                {
                                    case 'creditcard':
                                        
                                            finalPrice = finalPrice.toFixed(2)
                                            var apisendkey = $orderNo + "" + finalPrice;
                                            apisendkey = apiKeygen(apisendkey);
                                    
                                    
                                        
                                        var HttpURL = $globalServer + "/payment-paymaya?key="+apisendkey+"&requestreferencenumber="+$orderNo+"&ipaddress=125.60.148.241&mobile="+encodeURI(mobileno)+ "&email="+ encodeURI(emailadd) + "&firstname="+encodeURI(fullname)+ "&discount=0&servicecharge=0&shippingfee="+encodeURI(deliveryfee)+ "&tax=0&subtotal="+encodeURI(subtotal)+ "&value="+finalPrice;
                                        
                                        //Store URL for trying again
                                        sessionStorage.setItem("paymentAgain", HttpURL);
                                        
                                        request = new XMLHttpRequest();
                                        request.open("GET", HttpURL ,false);
                                        request.send();

                                        var data = JSON.parse(request.responseText);
                                        var $redirectURL = data.redirectUrl;
                                        var $code = data.code;

                                        
                                        console.log(data);
                                        
                                        
                                        if($code > 0){
                                            alert('Unknown server error, please contact customer support.');
                                        }else{
                                            setTimeout(function(){ 
                                            window.location.replace($redirectURL);
                                            },500);
                                        }
                                        
                                        
                                        
                                    break;
                                    default:
           
                                        //If the payment is COD                             
                                        setTimeout(function(){ 
                                            window.location.replace('5success.html');
                                        },1500);
                                        

                                    break;
                                }

                                
                            }else{
                                    document.getElementById('otpErrMsg').innerHTML = $message;
                                    document.getElementById('otpErrMsg').style.display = 'block';
                                    document.getElementById('confirmOTP').innerHTML = "Try Again!";
                                
                                
                                    if(bypass == 1){
                                        document.getElementById('formErrMsg').innerHTML = $message;
                                        document.getElementById('formErrMsg').style.display = 'block';
                                        document.getElementById('sendInfo').innerHTML = "Try Again! <span class='material-icons'>arrow_right_alt</span>";
                                    }
                                
                            }
                    
                    
                              
                    
                    
                }else{
                        document.getElementById('otpErrMsg').innerHTML = "Sorry! Mali ang iyong ibinigay na OTP code. Tignan ang text na aming ipinadala at i-try ulit.";
                        document.getElementById('otpErrMsg').style.display = 'block';
                        document.getElementById('confirmOTP').innerHTML = "Try Again!";
                }
    
    
}