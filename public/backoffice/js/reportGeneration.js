//window.addEventListener("load", loadaction);

function loadaction(){

    var dateQuery = dateToday();   
    document.getElementById("timestamp").innerText = new Date();
}





//-----------------------------

function showDeliveryReport(){
    loadaction();
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var dateSelected = sessionStorage.getItem('reportDate');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateSelected);
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=delivery_date&date=" + encodeURI(dateSelected) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
    
        var dataTable = `
                    <tr>
                        <th>Order No.</th>  
                        <th>Purchase Date</th>  
                        <th>Customer</th> 
                        <th>Mobile No.</th>
                        <th>Barangay / Zone</th>  
                        <th width='15%'>Delivery Address</th> 
                        <th>Delivery Mode</th>  
                        <th>Payment Mode</th>
                        <th width='12%'>Name of Driver</th>
                        <th width='8%'>Status</th>
                        <th width='8%'>AMOUNT PAYABLE</th>  
                    </tr>`;
    
    
        $totalOrders = $orderData.length;
            if($totalOrders == undefined || $totalOrders == NaN){
                $totalOrders = 0;
            }

    
    
        if($result == 'success'){
            
            for($x = 0; $x <$totalOrders; $x++)
            {
                
                var $orderNo = $orderData[$x]['orderno'];
                var $timestamp = $orderData[$x]['CreatedAt'];
                var $customer = $orderData[$x]['fullname'];
                var $mobile = $orderData[$x]['mobile'];
                var $fulladdress = $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
                var $barangay = $orderData[$x]['brgy'];
                var $paymenttype = $orderData[$x]['payment_mode'];
                var $deliverytype = $orderData[$x]['delivery_mode'];
                var $orderStatus = $orderData[$x]['status'];
                var $totalPrice = parseFloat($orderData[$x]['total_price']);
                    $totalPrice = $totalPrice.toFixed(2);
                
                
                
                if($deliverytype == 'delivery'){
                    
                    dataTable += `
                    <tr>
                        <td>`+ $orderNo +`</td>  
                        <td>`+ $timestamp +`</td>  
                        <td>`+ $customer +`</td>  
                        <td>`+ $mobile +`</td> 
                        <td>`+ $barangay +`</td>
                        <td>`+ $fulladdress +`</td>
                        <td>`+ $deliverytype +`</td>  
                        <td>`+ $paymenttype +`</td>
                        <td></td>
                        <td>`+ $orderStatus +`</td>
                        <td>`+ $totalPrice +`</td>
                    </tr>`;
                    
                }
                
                 
            }
            
            
            
        }else{
            
            //No Data Available
                dataTable += `
                <tr><td colspan='11'> No Available Data </td></tr>`;
        }
        
    
    
    document.getElementById("deliveydatetop").innerText = dateSelected;
    document.getElementById("printableTable").innerHTML = dataTable;
    
}

function showPickupReport(){
    loadaction();
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var dateSelected = sessionStorage.getItem('reportDate');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateSelected);
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=delivery_date&date=" + encodeURI(dateSelected) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
    
        console.log($orderData);
        var dataTable = `<tr>
                            <th>Order No.</th>  
                            <th>Purchase Date</th>  
                            <th>Customer</th> 
                            <th>Mobile No.</th>
                            <th>Barangay / Zone</th>  
                            <th width='15%'>Delivery Address</th> 
                            <th>Delivery Mode</th>  
                            <th>Payment Mode</th>
                            <th width='12%'>Name of Personnel</th>
                            <th width='8%'>Status</th>
                            <th width='8%'>AMOUNT PAYABLE</th>  
                        </tr>`;
    
    
        $totalOrders = $orderData.length;
            if($totalOrders == undefined || $totalOrders == NaN){
                $totalOrders = 0;
            }

    
    
        if($result == 'success'){
            
            for($x = 0; $x <$totalOrders; $x++)
            {
                
                var $orderNo = $orderData[$x]['orderno'];
                var $timestamp = $orderData[$x]['CreatedAt'];
                var $customer = $orderData[$x]['fullname'];
                var $mobile = $orderData[$x]['mobile'];
                var $fulladdress = $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
                var $barangay = $orderData[$x]['brgy'];
                var $paymenttype = $orderData[$x]['payment_mode'];
                var $deliverytype = $orderData[$x]['delivery_mode'];
                var $orderStatus = $orderData[$x]['status'];
                var $totalPrice = parseFloat($orderData[$x]['total_price']);
                    $totalPrice = $totalPrice.toFixed(2);
                
                
                
                if($deliverytype == 'pickup'){
                    
                    dataTable += `
                    <tr>
                        <td>`+ $orderNo +`</td>  
                        <td>`+ $timestamp +`</td>  
                        <td>`+ $customer +`</td>  
                        <td>`+ $mobile +`</td> 
                        <td>`+ $barangay +`</td>
                        <td>`+ $fulladdress +`</td>
                        <td>`+ $deliverytype +`</td>  
                        <td>`+ $paymenttype +`</td>
                        <td></td>
                        <td>`+ $orderStatus +`</td>
                        <td>`+ $totalPrice +`</td>
                    </tr>`;
                    
                }
                 
            }
            
            
            
        }else{
            
            //No Data Available
                dataTable += `
                <tr><td colspan='11'> No Available Data </td></tr>`;
        }
        
    
    
    document.getElementById("deliveydatetop").innerText = dateSelected;
    document.getElementById("printableTable").innerHTML = dataTable;
    
}

function showSalesReport(){
    loadaction();
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var dateSelected = sessionStorage.getItem('reportDate');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateSelected);
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=purchase_date&date=" + encodeURI(dateSelected) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
    
        console.log($orderData);
        var dataTable = `<tr>
                            <th>Purchase Date</th>  
                            <th>Delivery Date</th>  
                            <th>Order No.</th>  
                            <th width='20%'>Delivery Address</th> 
                            <th>Delivery Mode</th>  
                            <th>Payment Mode</th>
                            <th>Cost of Goods</th> 
                            <th>Delivery Fee</th>
                            <th>Payment Fee</th>
                            <th>Other Fees</th>
                            <th width='8%'>TOTAL</th>  
                        </tr>`;
    
        if($result == 'success'){
            
            $totalOrders = $orderData.length;
            if($totalOrders == undefined || $totalOrders == NaN){ $totalOrders = 0;}
            
            var totalSubtotal = 0;
            var totaldeliveryfee = 0;
            var totalpaymentfee = 0;
            var totalotherfee = 0;
            var totalGrandtotal = 0;
            
            
            for($x = 0; $x <$totalOrders; $x++)
            {
                
                var $purchaseDate = $orderData[$x]['purchase_date'];
                var $deliveryDate = $orderData[$x]['delivery_date'];
                var $orderNo = $orderData[$x]['orderno'];
                var $fulladdress = $orderData[$x]['fullname'] + "<br/>" + $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
                var $deliverytype = $orderData[$x]['delivery_mode'];
                var $paymenttype = $orderData[$x]['payment_mode'] + " <br/> " + $orderData[$x]['status'];
                var $subtotal = $orderData[$x]['subtotal'];
                var $deliveryfee = $orderData[$x]['delivery_fee'];
                var $paymentfee = $orderData[$x]['payment_fee'];
                var $otherfee = $orderData[$x]['other_charges'];
                    if($otherfee == ''){ $otherfee = 0;}
                var $totalPrice = parseFloat($orderData[$x]['total_price']);
                    $totalPrice = $totalPrice.toFixed(2);
                
                
                dataTable += `
                    <tr>
                        <td>`+ $purchaseDate +`</td>  
                        <td>`+ $deliveryDate +`</td>  
                        <td>`+ $orderNo +`</td>  
                        <td>`+ $fulladdress +`</td> 
                        <td>`+ $deliverytype +`</td>
                        <td>`+ $paymenttype +`</td>
                        <td>`+ $subtotal +`</td>  
                        <td>`+ $deliveryfee+`</td>
                        <td>`+ $paymentfee+`</td>
                        <td>`+ $otherfee +`</td>
                        <td>`+ $totalPrice +`</td>
                    </tr>`;

                //calculation for total
                totalSubtotal = totalSubtotal + parseFloat($subtotal);
                totaldeliveryfee = totaldeliveryfee + parseFloat($deliveryfee);
                totalpaymentfee = totalpaymentfee + parseFloat($paymentfee);
                totalotherfee = totalotherfee + parseFloat($otherfee);
                totalGrandtotal = totalGrandtotal + parseFloat($totalPrice);
                
                
            } //end of loop
            
            
            dataTable += `
                    <tr class='totalAmountTable'>
                        <td colspan='6'></td>  
                        <td>`+ totalSubtotal+`</td>  
                        <td>`+ totaldeliveryfee+`</td>
                        <td>`+ totalpaymentfee+`</td>
                        <td>`+ totalotherfee+`</td>
                        <td>`+ totalGrandtotal+`</td>
                    </tr>`;
            
            
    
            
            
            
        }else{
            
            //No Data Available
                dataTable += `
                <tr><td colspan='11'> No Available Data </td></tr>`;
        }
        
    
    
    document.getElementById("deliveydatetop").innerText = dateSelected;
    document.getElementById("printableTable").innerHTML = dataTable;
    
}

function showPrintableLabels(){
    
    
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var dateSelected = sessionStorage.getItem('reportDate');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateSelected);
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=delivery_date&date=" + encodeURI(dateSelected) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
        
        var labelContent = "";
    
for($x=0; $x<$orderData.length; $x++){    
    
        //Basic Order Information
        var orderNo = $orderData[$x]['orderno'];
    
        var paymentType = $orderData[$x]['payment_mode'];
            paymentType = paymentType.toUpperCase();
        var deliveryType = $orderData[$x]['delivery_mode'];
            deliveryType = deliveryType.toUpperCase();
        var paymentStatus = $orderData[$x]['status'];
    
        var fullname = $orderData[$x]['fullname'];
        var fulladdress = $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
        var mobile = $orderData[$x]['mobile'];
        var grandTotal = parseFloat($orderData[$x]['total_price']);
            grandTotal = grandTotal.toFixed(2);
        var deliverydate = $orderData[$x]['delivery_date'];
        
        //Full Order Information
        var subtotal = $orderData[$x]['subtotal'];
        var deliveryfee = parseFloat($orderData[$x]['delivery_fee']);
            deliveryfee = deliveryfee.toFixed(2);
        var paymentfee = parseFloat($orderData[$x]['payment_fee']);
            paymentfee = paymentfee.toFixed(2);
        var othercharges = parseFloat($orderData[$x]['other_charges']);
            othercharges = othercharges.toFixed(2);
        
        //Order Details / Loop
        var totalitems = 0;
        var orderList = $orderData[$x]['order_list'];
            orderList = JSON.parse(orderList);
        var summaryTable = "";
        var checklistTable = "";
    
        for($n = 0; $n<orderList.length; $n++)
        {
            
            var productName = orderList[$n]['productName'];
            var untqty = orderList[$n]['productQty'];
            var unitsubtotal = orderList[$n]['ProductPrice'];
            var unitPrice = unitsubtotal / untqty;
            totalitems++;
            
            if($n<17){
                    
            summaryTable +=`<tr>
                            <td>`+productName+`</td>    
                            <td>`+unitPrice+`</td>    
                            <td>`+untqty+`</td>    
                            <td>`+unitsubtotal+`</td>    
                            </tr>`;
                    
            }
            
            
            checklistTable +=`<tr>
                                <td>[__]</td>    
                                <td>`+productName+`</td>    
                                <td>`+unitPrice+`</td>    
                                <td>`+untqty+`</td>    
                                <td>`+unitsubtotal+`</td>    
                            </tr>`;

        }//end of list loop
        
        
    //Font Adjustments
    if(totalitems > 45){
        var customFont = 10;
    }else if(totalitems > 30){
        var customFont = 11;
    }else if(totalitems > 20){
        var customFont = 12;
    }else if(totalitems > 10){
        var customFont = 15;
    }else if(totalitems > 0){
        var customFont = 16;
    }
    
    //Show controller
    var $showContent = 1;
    
    if(paymentType == 'CREDITCARD' && paymentStatus != 'PAYMENT_SUCCESS'){ $showContent = 0; } 
    
//CONTENT BUILDING
   if($showContent == 1){
       
        labelContent += `<table id="mainTable">
        <tr>
        <td class="mainTD">
        <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div class="digiLogo">DIGIPALENGKE</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                    <div style="font-size: 22px; font-weight: bolder; margin-top: -10px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
            </div>
        <div class="row" style="padding: 5px; height: 720px;">
                <div class="col-8"><br/>
                    
                    <div style="font-size: 18px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+`<br/>
                    `+fulladdress+`<br/>
                    `+mobile+`
                    </div>
                    <div style="width: 100%; padding: 5px; text-align: center; border: solid 1px; margin-top: 20px; margin-bottom: 10px; font-weight: bold;">Summary of Items</div>
                    
                    <table width="100%" cellspacing="0" padding="0" style="text-align: left; margin: 7px;">
                    <tr>
                        <th width="50%">Product</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>Price</th>
                    </tr>
                        `+summaryTable+`
                        <tr>
                            <td colspan='4' align='center'>*** Will not display full order details. ****</td>    
                        </tr>
                    </table>
                </div>
                <div class="col-4" style="text-align: center;"><br/>
    <!--                   <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&color=555555&data=2320001" width="90%;">-->
                    <img src="img/qrcodesample.png" width="90%;">
                    
                    <div style="margin-top: 10px; font-size: 27px; font-weight: bold;">&#8369;`+ grandTotal +`</div>
                    <br/><br/>
                    <img src="img/noofbagbox.gif" width="80%;">
                    
                    <div style="margin-top: 10px; font-size: 20px;">
                    Delivery Date:<br/>
                    <strong>`+deliverydate+`</strong>
                    </div>
                    
                </div>
            </div>
            
        <div style="border-bottom: dashed 2px; height: 340px; margin-top: 10px" class="additionaltags">
            
             <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div style="font-size: 22px; font-weight: bolder; margin-top: 5px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                </div>
            </div>
            <div class="row" style="padding: 5px;">
                <div class="col-8"><br/>
                    
                    <div style="font-size: 18px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+`<br/>
                    `+fulladdress+`<br/>
                    `+mobile+`
                    </div> 
                </div>
                <div class="col-4" style="text-align: center;"><br/> 
                    <img src="img/noofbagbox.gif" height="130px">
                </div>
            </div>
            <div class="row" style="text-align: center; padding: 8px; font-size: 18px; font-weight: bold;">
                <div class="col-3">
                <img src="img/qrcodesample.png" height="80px">
                </div>
                <div class="col-5" style="padding-top: 20px; text-align: left;">
                    <img src="img/digipalengkecod.gif" width="70%;"><br/>
                    www.digipalengke.com
                </div>
                <div class="col-4" style="padding-top: 10px;">
                Delivery<br/>
                `+deliverydate+`
                </div>
            </div>
            
        </div>
        <div style="border-bottom: dashed 2px; height: 340px; margin-top: 10px" class="additionaltags">
             <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div style="font-size: 22px; font-weight: bolder; margin-top: 5px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                </div>
            </div>
            <div class="row" style="padding: 5px;">
                <div class="col-8"><br/>
                    <div style="font-size: 18px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+`<br/>
                    `+fulladdress+`<br/>
                    `+mobile+`
                    </div>
                </div>
                <div class="col-4" style="text-align: center;"><br/>
                    
                    <img src="img/noofbagbox.gif" height="130px">  
                </div>
            </div>
            <div class="row" style="text-align: center; padding: 8px; font-size: 18px; font-weight: bold;">
                <div class="col-3">
                <img src="img/qrcodesample.png" height="80px">
                </div>
                <div class="col-5" style="padding-top: 20px; text-align: left;">
                    <img src="img/digipalengkecod.gif" width="70%;"><br/>
                    www.digipalengke.com
                </div>
                <div class="col-4" style="padding-top: 10px;">
                Delivery<br/>
                `+deliverydate+`
                </div>
            </div>
            
        </div>
        <div style="border-bottom: dashed 2px; height: 340px; margin-top: 10px" class="additionaltags">
            
             <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div style="font-size: 22px; font-weight: bolder; margin-top: 5px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                </div>
            </div>
            <div class="row" style="padding: 5px;">
                <div class="col-8"><br/> 
                    <div style="font-size: 18px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+`<br/>
                    `+fulladdress+`<br/>
                    `+mobile+`
                    </div>      
                </div>
                <div class="col-4" style="text-align: center;"><br/>
                    
                    <img src="img/noofbagbox.gif" height="130px">  
                </div>
            </div>
            <div class="row" style="text-align: center; padding: 8px; font-size: 18px; font-weight: bold;">
                <div class="col-3">
                <img src="img/qrcodesample.png" height="80px">
                </div>
                <div class="col-5" style="padding-top: 20px; text-align: left;">
                    <img src="img/digipalengkecod.gif" width="70%;"><br/>
                    www.digipalengke.com
                </div>
                <div class="col-4" style="padding-top: 10px;">
                Delivery<br/>
                `+deliverydate+`
                </div>
            </div>
            
        </div>      
        </td>           
        <td class="mainTD">
            
            
    <div class="pickupList" style="margin-bottom: 20px;">
            
            
        <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div class="digiLogo">ORDER LIST</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                    <div style="font-size: 22px; font-weight: bolder; margin-top: -10px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
            </div>
        <div class="row" style="padding: 5px;">
                <div class="col-8"><br/>
                    
                    <div style="font-size: 15px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+`<br/>
                    `+fulladdress+`<br/>
                    `+mobile+`
                    </div>
                    
                    <div style="font-size: 16px; font-weight: 600; margin-top: 10px; border: 1px dashed; padding: 5px;">
                    NAME OF PICKER:
                    </div>
                    
                    <div style="width: 100%; padding: 5px; text-align: center; border: solid 1px; margin-top: 20px; margin-bottom: 10px; font-weight: bold;">Shopping List</div>
                    
                    <table width="100%" cellspacing="0" style="text-align: left; margin: 7px; font-size: `+customFont+`px;">
                    <tr>
                        <th></th>
                        <th width="50%">Product</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>Price</th>
                    </tr>
                    `+checklistTable+`
                    <tr style="text-align: center;">
                        <td colspan="5">***** NOTHING FOLLOWS ****</td>
                    </tr>
                    
                    
                    </table>
                </div>
                <div class="col-4" style="text-align: center;"><br/>
    <!--                   <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&color=555555&data=2320001" width="90%;">-->
                    <img src="img/qrcodesample.png" width="90%;">
                    
                    <div style="margin-top: 10px; font-size: 27px; font-weight: bold;">&#8369;`+ grandTotal +`</div>
                    <br/><br/>
                    <img src="img/noofbagbox.gif" width="80%;">
                    
                    <div style="margin-top: 10px; font-size: 20px;">
                    Delivery Date: <br/>
                    <strong>`+deliverydate+`</strong>
                    </div>
                    
                </div>
            </div>
            
            
    </div>  
            
    <div class="ordertracking">
        <div class="row" style="border: 2px solid; ">
                <div class="col-7">
                <div class="digiLogo">ACCOUNTING</div>
                </div>
                <div class="col-5" style="text-align: right; background-color: #dcdcdc;">
                    <div style="font-size: 30px;">#` + orderNo +`</div>
                    <div style="font-size: 22px; font-weight: bolder; margin-top: -10px;">` + deliveryType +` | ` + paymentType +`</div>
                </div>
            </div>
        <div class="row" style="padding: 5px; height: 720px;">
                <div class="col-8"><br/>
                    
                    <div style="font-size: 15px; font-weight: 600; text-transform: uppercase;">
                    `+fullname+` | 
                    `+fulladdress+` |
                    `+mobile+`
                    </div>
    
                    <div style="font-size: 16px; font-weight: 600; margin-top: 10px; border: 1px dashed; padding: 5px;">
                    DISPATCHER/BAGGER:
                    </div>
                    <div style="font-size: 16px; font-weight: 600; margin-top: 10px; border: 1px dashed; padding: 5px;">
                    CASHIER:
                    </div>
                    
                    <table width="100%" cellspacing="0" style="text-align: left; margin: 7px; font-size: 17px;">
                    <tr>
                        <th width="50%">Item</th>
                        <th></th>
                        <th>Total</th>
                    </tr> 
                    <tr>
                        <td>Transaction Price</td>    
                        <td>-</td>      
                        <td>`+ grandTotal +`</td>    
                    </tr>
                    <tr>
                        <td>Total Number of SKU</td>    
                        <td>-</td>      
                        <td>`+totalitems+`</td>    
                    </tr>
                    <tr>
                        <td>Delivery Fee</td>    
                        <td>-</td>      
                        <td>`+deliveryfee+`</td>    
                    </tr>
                    <tr>
                        <td>Payment Fee</td>    
                        <td>-</td>      
                        <td>`+paymentfee+`</td>    
                    </tr>
                    <tr>
                        <td>Other Charges</td>    
                        <td>-</td>      
                        <td>`+othercharges+`</td>    
                    </tr>
                    <tr >
                        <td colspan="4" style="font-weight: 600;">TOTAL PRICE: &#8369;`+ grandTotal +`</td>
                    </tr>
                    
                    
                    </table>
                </div>
                <div class="col-4" style="text-align: center;"><br/>
        <!--                   <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&color=555555&data=2320001" width="90%;">-->
                    <img src="img/qrcodesample.png" width="90%;">
                    
                    <div style="margin-top: 10px; font-size: 27px; font-weight: bold;">&#8369;`+ grandTotal +`</div>
                    <br/><br/>
                    <img src="img/noofbagbox.gif" width="80%;">
                    
                    <div style="margin-top: 10px; font-size: 20px;">
                    Delivery Date: <br/>
                    <strong>`+deliverydate+`</strong>
                    </div>
                    
                </div>
            </div>   
                </div>  
            </td>
        </tr>
    </table>`;
   
   }//end of show content checker
       
}//end of main loop
    
    document.getElementById("printableLabel").innerHTML = labelContent;
  
}

