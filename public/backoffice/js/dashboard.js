window.addEventListener("load", loadaction);
document.getElementById("tomorrowBtn").addEventListener("click", function(){dataView('tomorrow')});
document.getElementById("yesterdayBtn").addEventListener("click", function(){dataView('yesterday')});
document.getElementById("todayBtn").addEventListener("click", function(){dataView('today')});
document.getElementById("generateReportAction").addEventListener("click", generateReport);




function loadaction(){

    var dateQuery = dateToday();    
        showTable(dateQuery);
    
}

function dataView(selection){
    
        switch(selection){
            case 'tomorrow':
                var dateQuery = dateToday(1);   
                document.getElementById("yesterdayBtn").style.backgroundColor = "#adadad";
                document.getElementById("todayBtn").style.backgroundColor = "#adadad";
                document.getElementById("tomorrowBtn").style.backgroundColor = "#C62828";
            break;
            case 'yesterday':
                var dateQuery = dateToday(-1);   
                document.getElementById("yesterdayBtn").style.backgroundColor = "#C62828";
                document.getElementById("todayBtn").style.backgroundColor = "#adadad";
                document.getElementById("tomorrowBtn").style.backgroundColor = "#adadad";
            break;
            default:
                var dateQuery = dateToday();   
                document.getElementById("yesterdayBtn").style.backgroundColor = "#adadad";
                document.getElementById("todayBtn").style.backgroundColor = "#C62828";
                document.getElementById("tomorrowBtn").style.backgroundColor = "#adadad";
            break;
        }
    
    showTable(dateQuery);
    
}
function showTable(dateQuery){

    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateQuery);
    
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=delivery_date&date=" + encodeURI(dateQuery) + "&key=" + encodeURI(key) + "";
    
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
    
    
        var dataTable = `
                <tr><th>Order No.</th>
                    <th>Customer</th>
                    <th>Contact No</th>
                    <th>Address</th>
                    <th>Payment</th>
                    <th>Delivery</th>
                    <th>Total</th></tr>`;
    
    
        $totalOrders = $orderData.length;
            if($totalOrders == undefined || $totalOrders == NaN){
                $totalOrders = 0;
            }
        $totalRevenue = 0.00;    
    
    
        if($result == 'success'){
            
            for($x = 0; $x <$totalOrders; $x++)
            {
                
                var $orderNo = $orderData[$x]['orderno'];
                var $timestamp = $orderData[$x]['CreatedAt'];
                var $customer = $orderData[$x]['fullname'];
                var $mobile = $orderData[$x]['mobile'];
                var $fulladdress = $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
                var $paymenttype = $orderData[$x]['payment_mode'];
                var $deliverytype = $orderData[$x]['delivery_mode'];
                var $totalPrice = parseFloat($orderData[$x]['total_price']);
                    $totalPrice = $totalPrice.toFixed(2);
                
                dataTable += `
                <tr>
                    <td><span class='orderNobelow' onclick="showOrder('`+ $orderNo +`')">`+ $orderNo +`</span><br/><span class='orderDate'>`+ $timestamp +`</span></td>
                    <td>`+ $customer +`</td>
                    <td>`+ $mobile +`</td>
                    <td class='add'>`+ $fulladdress +`</td>
                    <td>`+ $paymenttype +`</td>
                    <td>`+ $deliverytype +`</td>
                    <td>&#x20B1;`+ $totalPrice +`</td>
                </tr>`;
                
                $totalRevenue = $totalRevenue + parseFloat($totalPrice);
                 
            }
            
            $totalRevenue = $totalRevenue.toFixed(2);
            
            
            
        }else{
            
            //No Data Available
                dataTable += `
                <tr><td colspan='7'> No Available Data </td></tr>`;
        }
        
     document.getElementById("dataTable").innerHTML = dataTable;
     document.getElementById("salesAmount").innerText = $totalRevenue;
     document.getElementById("salesCount").innerText = $totalOrders;
    
}
function showOrder(orderNo){
    
    var storekey = sessionStorage.getItem('storekey');
    var key = apiKeygen(orderNo + "" + storekey);
    
    var HttpURL = $globalServer + "/getorderdetails?storekey=" + encodeURI(storekey) + "&orderno=" + encodeURI(orderNo) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
    
        if($result == 'success'){

            var $orderDetails = data.data;
            var $orderDetails = JSON.parse($orderDetails['order_list']);
            
            console.log($orderDetails);
            
            var $tableContent = `<tr>
                    <th width='50%'>Product</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>`;
            
            for($x = 0; $x < $orderDetails.length; $x++){
                var productName = $orderDetails[$x]['productName'];
                var productQty = $orderDetails[$x]['productQty'];
                var productPrice = $orderDetails[$x]['ProductPrice'];
                
                $tableContent += `<tr>
                    <td>`+ productName +`</td>
                    <td>`+ productQty +`</td>
                    <td>`+ productPrice +`</td>
                </tr>`;
            }
            
            document.getElementById("orderDetailsTable").innerHTML = $tableContent;
            document.getElementById("orderNoDisplay").innerText = orderNo;
            
            $("#viewOrder").modal("show");
            
        }else{
            alert('Failed attempt to recover order details.');
        }

}
function generateReport(){
    
    var selectedDay = document.getElementById("reportDate").value; 
    var selectedReport = document.getElementById("reportType").value; 
    
        //Convert selection to date
        switch(selectedDay){
            case 'Tomorrow':
                var dateQuery = dateToday(1);   
                sessionStorage.setItem('reportDate', dateQuery);
            break;
            case 'Yesterday':
                var dateQuery = dateToday(-1);   
                sessionStorage.setItem('reportDate', dateQuery);
            break;
            default:
                var dateQuery = dateToday();   
                sessionStorage.setItem('reportDate', dateQuery);
            break;
        }
        switch(selectedReport){
            case 'Admin Printable Labels':
                window.open("reports/orderlabels.html", "_blank");
            break;
            case 'Admin Sales Report':
                window.open("reports/adminsales.html", "_blank");
            break;
            case 'Delivery Report':
                window.open("reports/admindelivery.html", "_blank");
            break;
            case 'Pickup Report':
                window.open("reports/adminpickup.html", "_blank");
            break;
            case 'Downloadable CSV Report':
                downloadFile();
            break;
            default:
               alert('Unknown Server Error');
               window.location.replace("index.html");
            break;
        }

}


//-------- CSV Generation Report -----//

function obj2csv(obj, opt) {
        if (typeof obj !== 'object') return null;
        opt = opt || {};
        var scopechar = opt.scopechar || '/';
        var delimeter = opt.delimeter || ',';
        if (Array.isArray(obj) === false) obj = [obj];
        var curs, name, rownum, key, queue, values = [], rows = [], headers = {}, headersArr = [];
        for (rownum = 0; rownum < obj.length; rownum++) {
            queue = [obj[rownum], ''];
            rows[rownum] = {};
            while (queue.length > 0) {
                name = queue.pop();
                curs = queue.pop();
                if (curs !== null && typeof curs === 'object') {
                    for (key in curs) {
                        if (curs.hasOwnProperty(key)) {
                            queue.push(curs[key]);
                            queue.push(name + (name ? scopechar : '') + key);
                        }
                    }
                } else {
                    if (headers[name] === undefined) headers[name] = true;
                    rows[rownum][name] = curs;
                }
            }
            values[rownum] = [];
        }
        // create csv text
        for (key in headers) {
            if (headers.hasOwnProperty(key)) {
                headersArr.push(key);
                for (rownum = 0; rownum < obj.length; rownum++) {
                    values[rownum].push(rows[rownum][key] === undefined
                        ? ''
                        : JSON.stringify(rows[rownum][key]));
                }
            }
        }
        for (rownum = 0; rownum < obj.length; rownum++) {
            values[rownum] = values[rownum].join(delimeter);
        }
        return '"' + headersArr.join('"' + delimeter + '"') + '"\n' + values.join('\n');
    }
function downloadFile(){
    
    
      var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var dateSelected = sessionStorage.getItem('reportDate');
    var key = apiKeygen(storekey +""+ loginkey +""+ dateSelected);
    
    var HttpURL = $globalServer + "/getorderlist?storekey=" + encodeURI(storekey) + "&userid=" + encodeURI(loginkey) + "&filter=purchase_date&date=" + encodeURI(dateSelected) + "&key=" + encodeURI(key) + "";
    
        console.log(HttpURL);
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.response;
        var $orderData = data.data;
    
        console.log($orderData);
        var dataTable = new Array();
        dataTable[0] =  ["Purchase Date", "Delivery Date","Order No.", "Delivery Address", "Delivery Mode", "Payment Mode", "Cost of Goods", "Delivery Fee", "Payment Fee", "Other Fees", "TOTAL"];
    
    
        if($result == 'success'){
            
            $totalOrders = $orderData.length;
            if($totalOrders == undefined || $totalOrders == NaN){ $totalOrders = 0;}
            
            var totalSubtotal = 0;
            var totaldeliveryfee = 0;
            var totalpaymentfee = 0;
            var totalotherfee = 0;
            var totalGrandtotal = 0;
            
            
            for($x = 0; $x <$totalOrders; $x++)
            {
                $row = $x + 1;
                var $purchaseDate = $orderData[$x]['purchase_date'];
                var $deliveryDate = $orderData[$x]['delivery_date'];
                var $orderNo = $orderData[$x]['orderno'];
                var $fulladdress = $orderData[$x]['fullname'] + "<br/>" + $orderData[$x]['unit_no'] + " " + $orderData[$x]['street'] + ", " + $orderData[$x]['brgy'] + "" + $orderData[$x]['city'] + " (" + $orderData[$x]['landmark'] + ")";
                var $deliverytype = $orderData[$x]['delivery_mode'];
                var $paymenttype = $orderData[$x]['payment_mode'] + " - " + $orderData[$x]['status'];
                var $subtotal = $orderData[$x]['subtotal'];
                var $deliveryfee = $orderData[$x]['delivery_fee'];
                var $paymentfee = $orderData[$x]['payment_fee'];
                var $otherfee = $orderData[$x]['other_charges'];
                    if($otherfee == ''){ $otherfee = 0;}
                var $totalPrice = $orderData[$x]['total_price'];
                
                
                dataTable[$row] =  [$purchaseDate, $deliveryDate,$orderNo, $fulladdress, $deliverytype, $paymenttype, $subtotal, $deliveryfee, $paymentfee, $otherfee, $totalPrice];
    
            }
 
            var $csvValue = obj2csv(dataTable);
            
          var element = document.createElement('a');
          element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent($csvValue));
          element.setAttribute('download', 'digipalengke-salesreport.csv');

          element.style.display = 'none';
          document.body.appendChild(element);
          element.click();
          document.body.removeChild(element);
          window.close();
            
    }else{
        alert("Unknown server error!");
    }
    
    
}


//-------- END: CSV Generation Report -----//