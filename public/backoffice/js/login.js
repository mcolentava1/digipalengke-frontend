document.getElementById("signinbutton").addEventListener("click", loginuser);

function loginuser(){
    
    
     //Loading screen
    document.getElementById('signinbutton').innerHTML = "<img src='../img/loading.svg' height='18px'>";
    
    $username = document.getElementById("username").value;
    $password = document.getElementById("password").value;
    
    
    var HttpURL = $globalServer + "/login?username=" + encodeURI($username)+"&password=" + encodeURI($password)+"";
        
    
    
    setTimeout(function(){ 
                                        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();

        var data = JSON.parse(request.responseText);
        var $result = data.result;

        if($result == 'success'){


            var $loginkey = data.loginkey;
            var $storekey = data.storekey;
            var $validator = apiKeygen($loginkey + "" + $storekey); 
                //loginkey + storekey

            sessionStorage.setItem('storekey', $storekey);
            sessionStorage.setItem('loginkey', $loginkey);
            sessionStorage.setItem('validator', $validator);
            sessionStorage.setItem('user', $username);
            window.location.replace("dashboard.html");

        }else{
            
            document.getElementById("errormsg").style.display = 'block';
            document.getElementById('signinbutton').innerHTML = "Sign In";
            
        }
    
    },500);
    
    
    
    
}

