
window.addEventListener("load", loadProducts);

function loadProducts(){
    
    //Fetch product value
    var storekey = sessionStorage.getItem('storekey');
    var HttpURL = $globalServer + "/getproductlist?storekey=" + encodeURI(storekey)+"&key=";
        

            request = new XMLHttpRequest();
            request.open("GET", HttpURL ,false);
            request.send();
                
            var data = JSON.parse(request.responseText);
            var $response = data.response;
            const product = data.data;
    
            if($response == 'failed'){
                var productCount = 0;
            }else{
                var productCount = product.length;
            }
    
  
  
    //Load Category Variables
            {
                var staples_menu = "";
                var meat_menu = "";
                var seafood_menu = "";
                var fruits_menu = "";
                var condiments_menu = "";
                var packedgoods_menu = "";
                var hygiene_menu = "";
                var baby_menu = "";
                var cleaning_menu = "";
                var specials_menu = "";  
            }
            
          //Load Product Units
          for(i=0 ; i < productCount || i>1000; i++){
                var variableNo = i + 1;
              
                //Load Product Units
                var productCategory = product[i]['category'];
                
                var productName = product[i]['productname'];
                var productBrand = product[i]['brand'];
                var productStock = product[i]['stock'];
                var productUnit = product[i]['unit'];
                var productPrice = product[i]['price'];
                var productID = product[i]['productkey'];
              
                switch(productCategory){
                    case "staples":
                        staples_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'meat':
                        meat_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'seafood':
                        seafood_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'fruits':
                        fruits_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'condiments':
                        condiments_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'packedgoods':
                        packedgoods_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'hygiene':
                        hygiene_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'baby':
                        baby_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'cleaning':
                        cleaning_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    case 'specials':
                        specials_menu += `
                            <tr>
                            <td>`+ productName +`</td>
                            <td>`+productBrand+`</td>
                            <td>`+productStock+`</td>
                            <td>`+productUnit+`</td>
                            <td>&#8369;`+productPrice+`</td>
                            <td>
                                <span class="material-icons actionIcon" onclick="deleteProduct('`+productID+`')">delete</span>
                                <span class="material-icons actionIcon" onclick="startEdit('`+i+`')">edit</span>
                            </td>
                        </tr>
                        `;
                    break;
                    default:
                        alert('Unknown server error! Contact system admin.');
                    break;
                }
            
          }//end of product loop
    
        var tableHeader = "<tr><th>Product</th><th>Brand</th><th>Stock</th><th>Unit</th><th>Price</th><th width='15%'></th></tr>";
        var emptyTable = tableHeader + "<tr><td colspan='6'>No product available</td></tr>";
        
    
        if(staples_menu != "") { document.getElementById('staples_menu').innerHTML = tableHeader +""+ staples_menu; }else{ document.getElementById('staples_menu').innerHTML = emptyTable; }
    
        if(meat_menu != "") { document.getElementById('meat_menu').innerHTML = tableHeader +""+ meat_menu; }else{ document.getElementById('meat_menu').innerHTML = emptyTable; }


        if(seafood_menu != "") { document.getElementById('seafood_menu').innerHTML = tableHeader +""+ seafood_menu; }else{ document.getElementById('seafood_menu').innerHTML = emptyTable; }

        if(fruits_menu != "") { document.getElementById('fruits_menu').innerHTML = tableHeader +""+ fruits_menu; }else{ document.getElementById('fruits_menu').innerHTML = emptyTable; }

        if(condiments_menu != "") { document.getElementById('condiments_menu').innerHTML = tableHeader +""+ condiments_menu; }else{ document.getElementById('condiments_menu').innerHTML = emptyTable;}

        if(packedgoods_menu != "") { document.getElementById('packedgoods_menu').innerHTML = tableHeader +""+ packedgoods_menu; }else{ document.getElementById('packedgoods_menu').innerHTML = emptyTable; }

        if(hygiene_menu != "") { document.getElementById('hygiene_menu').innerHTML = tableHeader +""+ hygiene_menu; }else{ document.getElementById('hygiene_menu').innerHTML = emptyTable;}

        if(baby_menu != "") { document.getElementById('baby_menu').innerHTML = tableHeader +""+ baby_menu; }else{ document.getElementById('baby_menu').innerHTML = emptyTable; }    

        if(cleaning_menu != "") { document.getElementById('cleaning_menu').innerHTML = tableHeader +""+ cleaning_menu; }else{ document.getElementById('cleaning_menu').innerHTML = emptyTable; }

        if(specials_menu != "") { document.getElementById('specials_menu').innerHTML = tableHeader +""+ specials_menu; }else{ document.getElementById('specials_menu').innerHTML = emptyTable; }
}

function addProduct(){
    
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    
    
    var category = document.getElementById("dash_cat").value;
    var productname = document.getElementById("dash_name").value;
    var brand = document.getElementById("dash_brand").value;
    var unit = document.getElementById("dash_unit").value;
    var price = document.getElementById("dash_price").value;
    var stock = document.getElementById("dash_stock").value;
    var increment = document.getElementById("dash_increment").value;
    var max = document.getElementById("dash_max").value;
    var description = document.getElementById("dash_description").value;
    
     var HttpURL = $globalServer + "/createproduct?category=" + encodeURI(category)+"&productname=" + encodeURI(productname)+"&description=" + encodeURI(description)+"&brand=" + encodeURI(brand)+"&price=" + encodeURI(price)+"&unit=" + encodeURI(unit)+"&increment=" + encodeURI(increment)+"&maxqty=" + encodeURI(max)+"&stock=" + encodeURI(stock)+"&storekey=" + encodeURI(storekey) +"";
        
      request = new XMLHttpRequest();
      request.open("GET", HttpURL ,false);
      request.send();

      var data = JSON.parse(request.responseText);
      var $result = data.response;
      var $message = data.message;

        if($result == 'success'){
            loadProducts();
            
            alert("Added successfully!");
            $('#addProduct').modal('hide');
            
            
            document.getElementById("dash_cat").value = "";
            document.getElementById("dash_name").value = "";
            document.getElementById("dash_brand").value = "";
            document.getElementById("dash_unit").value = "";
            document.getElementById("dash_price").value = "";
            document.getElementById("dash_stock").value = "";
            document.getElementById("dash_increment").value = "";
            document.getElementById("dash_max").value = "";
            document.getElementById("dash_description").value = "";
            
        }else{
            alert($message);
        }
    
}

function deleteProduct(productID){
    
    
    //todo: use bootstrap version
    var response = confirm("Are you sure you want to delete a product?");
      if (response == true) {

          var storekey = sessionStorage.getItem('storekey');
          var loginkey = sessionStorage.getItem('loginkey');
          
          var HttpURL = $globalServer + "/deleteproduct?storekey="+storekey+"&productkey="+productID+"&loginkey="+loginkey+"&key=";
        
          request = new XMLHttpRequest();
          request.open("GET", HttpURL ,false);
          request.send();

          var data = JSON.parse(request.responseText);
          var $result = data.response;
          var $message = data.message;

          if($result == 'success'){
              loadProducts();
              //setTimeout(function(){ alert("Deleted successfully."); }, 500);
          }else{
              alert("Error: " + $message);
          }
          
      }

    
}

function startEdit(productIndex){
    
        //Fetch product value
        var storekey = sessionStorage.getItem('storekey');
        var HttpURL = $globalServer + "/getproductlist?storekey=" + encodeURI(storekey)+"&key=";
        

            request = new XMLHttpRequest();
            request.open("GET", HttpURL ,false);
            request.send();
                
            var data = JSON.parse(request.responseText);
            var $response = data.response;
            const product = data.data;

                var productCategory = product[productIndex]['category'];
                var productName = product[productIndex]['productname'];
                var productBrand = product[productIndex]['brand'];
                var productStock = product[productIndex]['stock'];
                var productUnit = product[productIndex]['unit'];
                var productPrice = product[productIndex]['price'];
                var productIncrement = product[productIndex]['increment'];
                var productMax = product[productIndex]['maxqty'];
                var productID = product[productIndex]['productkey'];
                var productDescription = product[productIndex]['description'];
                var productKey = product[productIndex]['productkey'];
    
                    switch(productCategory){
                    case 'staples':
                        var catIndex = 0;
                    break;
                    case 'meat':
                        var catIndex = 1;
                    break;
                    case 'seafood':
                        var catIndex = 2;
                    break;
                    case 'fruits':
                        var catIndex = 3;
                    break;
                    case 'condiments':
                        var catIndex = 4;
                    break;
                    case 'packedgoods':
                        var catIndex = 5;
                    break;
                    case 'hygiene':
                        var catIndex = 6;
                    break;
                    case 'baby':
                        var catIndex = 7;
                    break;
                    case 'cleaning':
                        var catIndex = 8;
                    break;
                    case 'specials':
                        var catIndex = 9;
                    break;  
                }

                document.getElementById("edit_name").value = productName;
                document.getElementById("edit_brand").value = productBrand;
                document.getElementById("edit_unit").value = productUnit;
                document.getElementById("edit_price").value = productPrice;
                document.getElementById("edit_stock").value = productStock;
                document.getElementById("edit_increment").value = productIncrement;
                document.getElementById("edit_max").value = productMax;
                document.getElementById("edit_description").value = productDescription;
                document.getElementById("edit_cat").selectedIndex = catIndex;
                document.getElementById("edit_prodkey").value = productKey;

    
    $('#editProduct').modal('show');
    //loadProducts();
}

function editProduct(){
    
    var storekey = sessionStorage.getItem('storekey');
    var loginkey = sessionStorage.getItem('loginkey');
    var user = sessionStorage.getItem('user');
    var productKey = document.getElementById("edit_prodkey").value;
    
    var category = document.getElementById("edit_cat").value;
    var productname = document.getElementById("edit_name").value;
    var brand = document.getElementById("edit_brand").value;
    var unit = document.getElementById("edit_unit").value;
    var price = document.getElementById("edit_price").value;
    var stock = document.getElementById("edit_stock").value;
    var increment = document.getElementById("edit_increment").value;
    var max = document.getElementById("edit_max").value;
    var description = document.getElementById("edit_description").value;
    
     var HttpURL = $globalServer + "/editproduct?category=" + encodeURI(category)+"&productkey="+encodeURI(productKey)+"&productname=" + encodeURI(productname)+"&description=" + encodeURI(description)+"&brand=" + encodeURI(brand)+"&price=" + encodeURI(price)+"&unit=" + encodeURI(unit)+"&increment=" + encodeURI(increment)+"&maxqty=" + encodeURI(max)+"&stock=" + encodeURI(stock)+"&storekey=" + encodeURI(storekey) +"";
        
      request = new XMLHttpRequest();
      request.open("GET", HttpURL ,false);
      request.send();

      var data = JSON.parse(request.responseText);
      var $result = data.response;
      var $message = data.message;

        if($result == 'success'){
            $('#editProduct').modal('hide');
            setTimeout(function(){ loadProducts(); }, 700);
            //console.log("Success updating " + productKey);
        }else{
            alert($message);
        }
    
    
}