//Last Update 05/25/2020
//==========  START: EVENTS  ===============//

window.addEventListener("load", loadProducts);
document.getElementById("checkoutBtn").addEventListener("click", checkout);

//==========  START: FUNCTIONS  ==========//


function loadProducts() {
        
    
        //Set ordering limit per delivery date.
        limitShop();
        setInterval(limitShop, 120000);
    
        //Load Initial Values
            localStorage.setItem("TotalPrice", 0);
            localStorage.setItem("totalQty", 0);
            localStorage.setItem("totalVariance", 0);
            localStorage.setItem("finalPrice", 0);
            
        //Load Category Variables
            {
                var staples_menu = "";
                var meat_menu = "";
                var seafood_menu = "";
                var fruits_menu = "";
                var condiments_menu = "";
                var packedgoods_menu = "";
                var hygiene_menu = "";
                var baby_menu = "";
                var cleaning_menu = "";
                var specials_menu = "";  
            }
            
          //Load Product Units
          for(i=0 ; i < productCount || i>1000; i++){
                var variableNo = i + 1;
              
                //Load Product Units
                var productCategory = product[i]['category'];
                var productUnit = product[i]['unit'];
                var productName = product[i]['productname'];
                var productBrand = product[i]['brand'];
                var productStock = product[i]['stock'];
                var productIncrement = product[i]['increment'];
              
            if(productStock != 0 && productStock >= productIncrement){
                  
                  switch(productCategory){
                    case "staples":
                        staples_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'meat':
                        meat_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'seafood':
                        seafood_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'fruits':
                        fruits_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'condiments':
                        condiments_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'packedgoods':
                        packedgoods_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+` </span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'hygiene':
                        hygiene_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'baby':
                        baby_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'cleaning':
                        cleaning_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    case 'specials':
                        specials_menu += `
                            <tr>
                                <td id="nameDisplay_` + i + `">`+ productName +` <span class='brand'>`+productBrand+`</span></td>
                                <td class='cb'>
                                    <div class="col1"><div class="counter" onclick="SubQTY(` + i + `)">-</div></div>
                                    <div class="col2">
                                        <span id="qtyDisplay_` + i + `">0</span>
                                        <span id="unitDisplay_` + i + `">`+productUnit+`</span>
                                    </div>
                                    <div class="col3">
                                        &#8369;<span id="priceDisplay_` + i + `">0.00</span>
                                    </div>
                                    <div class="col4"><div class="counter" onclick="AddQTY(` + i + `)">+</div></div>
                                </td>
                              </tr>
                        `;
                    break;
                    default:
                        alert('Unknown server error! Contact system admin.');
                    break;
                }
            }
                
            
          }//end of product loop
    
    
        if(staples_menu != "") { 
            document.getElementById('staples_menu').innerHTML = staples_menu;
            document.getElementById('staples_container').style.display = 'block';
        }else{ document.getElementById('staples_container').style.display = 'none'; }
    
        if(meat_menu != "") { 
            document.getElementById('meat_menu').innerHTML = meat_menu; 
            document.getElementById('meat_container').style.display = 'block';
        }else{ document.getElementById('meat_container').style.display = 'none'; }


        if(seafood_menu != "") { 
            document.getElementById('seafood_menu').innerHTML = seafood_menu; 
            document.getElementById('seafood_container').style.display = 'block';
        }else{ document.getElementById('seafood_container').style.display = 'none'; }

        if(fruits_menu != "") { 
            document.getElementById('fruits_menu').innerHTML = fruits_menu; 
            document.getElementById('fruits_container').style.display = 'block';
        }else{ document.getElementById('fruits_container').style.display = 'none'; }

        if(condiments_menu != "") { 
            document.getElementById('condiments_menu').innerHTML = condiments_menu; 
            document.getElementById('condiments_container').style.display = 'block';
        }else{ document.getElementById('condiments_container').style.display = 'none'; }

        if(packedgoods_menu != "") { 
            document.getElementById('packedgoods_menu').innerHTML = packedgoods_menu; 
            document.getElementById('packedgoods_container').style.display = 'block';
        }else{ document.getElementById('packedgoods_container').style.display = 'none'; }

        if(hygiene_menu != "") { 
            document.getElementById('hygiene_menu').innerHTML = hygiene_menu; 
            document.getElementById('hygiene_container').style.display = 'block';
        }else{ document.getElementById('hygiene_container').style.display = 'none'; }

        if(baby_menu != "") { 
            document.getElementById('baby_menu').innerHTML = baby_menu; 
            document.getElementById('baby_container').style.display = 'block';
        }else{ document.getElementById('baby_container').style.display = 'none'; }    

        if(cleaning_menu != "") { 
            document.getElementById('cleaning_menu').innerHTML = cleaning_menu; 
            document.getElementById('cleaning_container').style.display = 'block';
        }else{ document.getElementById('cleaning_container').style.display = 'none'; }

        if(specials_menu != "") { 
            document.getElementById('specials_menu').innerHTML = specials_menu; 
            document.getElementById('specials_container').style.display = 'block';
        }else{ document.getElementById('specials_container').style.display = 'none'; }
    
    document.getElementById('loadingcontainer').style.display = 'none';
    
}
function AddQTY(prodNum){
    //Getting total amount 
    var totalPrice = localStorage.getItem('TotalPrice'); 
    var maxTotalPRice = 3000;
    
    
    //Getting previous records
   var arrayNum = prodNum;
   var orderQtyVar = "ORDER_qty" + prodNum;
        var qtyDisplay = "qtyDisplay_" + prodNum;
   var orderPriceVar = "ORDER_price" + prodNum;
        var priceDisplay = "priceDisplay_" + prodNum;
    var orderIDVar = "ORDER_id" + prodNum;
    var orderNameVar = "ORDER_name" + prodNum;
    
   var price = product[arrayNum]['price']; price = parseFloat(price);
   var max = product[arrayNum]['maxqty'];  max = parseFloat(max);
   var stock = product[arrayNum]['stock']; stock = parseFloat(stock);
   var increment = product[arrayNum]['increment']; increment = parseFloat(increment);
   var productID = product[arrayNum]['ID']; 
   var productName = product[arrayNum]['productname']; 
    
   var currentQty = localStorage.getItem(orderQtyVar); currentQty = parseFloat(currentQty);
       if(isNaN(currentQty)){ currentQty = 0; }
    
    //Pre-processing additional value
    var newQty = currentQty + increment;
    var newPrice = newQty * price;
    
    if(totalPrice > maxTotalPRice){
        
        alert("Oops! Naabot na ang limit ng presyo para sa order na ito.");
        
    }else{
        
        //Checking value qualification
        if(newQty <= max && newQty <= stock ){

                //Processing if successful
                localStorage.setItem(orderQtyVar, newQty);
                document.getElementById(qtyDisplay).innerText = newQty;

                localStorage.setItem(orderPriceVar, newPrice);
                document.getElementById(priceDisplay).innerText = newPrice.toFixed(2);
            
                localStorage.setItem(orderIDVar, productID);
                localStorage.setItem(orderNameVar, productName);
            
            
                //After Process
                totalAmount();

        }else{
            alert("Oops! Naabot mo na ang maximum na bilang ng produkto para sa order na ito.");
        }
        
    }
    
    
}
function SubQTY(prodNum){
    
    
   //Getting previous records
   var arrayNum = prodNum;
   var orderQtyVar = "ORDER_qty" + prodNum;
        var qtyDisplay = "qtyDisplay_" + prodNum;
   var orderPriceVar = "ORDER_price" + prodNum;
        var priceDisplay = "priceDisplay_" + prodNum;
    
   var price = product[arrayNum]['price']; price = parseFloat(price);
   var max = product[arrayNum]['maxqty'];  max = parseFloat(max);
   var stock = product[arrayNum]['stock']; stock = parseFloat(stock);
   var increment = product[arrayNum]['increment']; increment = parseFloat(increment);
   var currentQty = localStorage.getItem(orderQtyVar); currentQty = parseFloat(currentQty);
       if(isNaN(currentQty)){ currentQty = 0; }
    
    //Pre-processing additional value
    var newQty = currentQty - increment;
    var newPrice = newQty * price;
    
    //Checking value qualification
    if(newQty >= 0){
        
            //Processing if successful
            localStorage.setItem(orderQtyVar, newQty);
            document.getElementById(qtyDisplay).innerText = newQty;
    
            localStorage.setItem(orderPriceVar, newPrice);
            document.getElementById(priceDisplay).innerText = newPrice.toFixed(2);
        
    }
    
    //After Process
    totalAmount();
    
}
function totalAmount(){
    
    var totalPrice = 0;
    var totalProduct = 0;
    var totalQty = 0;
    var receipt = "";
    
    for(i=0 ; i < productCount || i>3000;i++){

        
                var storageName = "ORDER_qty" + i;
                var productQty = localStorage.getItem(storageName); productQty = parseFloat(productQty);
                    if(isNaN(productQty)){ productQty = 0; }
                var productPrice = product[i]['price']; productPrice = parseFloat(productPrice);
                var productName = product[i]['productname']; productName = parseFloat(productName);
        
                totalPrice = totalPrice + (productPrice * productQty);
                totalQty = totalQty + productQty;
                if(productQty > 0){totalProduct = totalProduct + 1;}
                  
          }
    //End Process    
    localStorage.setItem('TotalPrice', totalPrice);
    localStorage.setItem('totalQty', totalQty);
    localStorage.setItem('totalVariance', totalProduct);

    document.getElementById('subtotal').innerText = addComma(totalPrice.toFixed(2));
    priceEncrypt(totalPrice, totalQty); //important decrypting
}
function checkout(){
    
    
    document.getElementById('checkoutBtn').innerHTML = "<img src='../img/loading.svg' height='18px'>";
    
    setTimeout(function(){ 
    
        var totalPrice = localStorage.getItem('TotalPrice'); 

        if(totalPrice > 100000){
            document.getElementById('otpErrMsg').style.display = "block";
            document.getElementById('otpErrMsg').innerHTML = "Sorry, sa kasalukuyan ay mayroon tayong limit na &#8369; 1,500 para sa mga order sa DigiPalengke. Tayo po ay magtipid sa pagkain para sa patuloy na supply sa panahon ng krisis.";
            
            document.getElementById('checkoutBtn').innerHTML = "Try Again";
            
        }else if(totalPrice < 100){
            document.getElementById('otpErrMsg').style.display = "block";
            document.getElementById('otpErrMsg').innerHTML = "Sorry, ang minimum order sa DigiPalengke ay &#8369; 100.00 para makatipid sa delivery.";
            document.getElementById('checkoutBtn').innerHTML = "Try Again";
        }else{
            window.location.replace('3checkout.html');
        }

    
    },1000);

    
}


