openapi: 3.0.0
servers:
  - description: staging
    url: https://digipalengke.herokuapp.com/api/v1
info:
  title: BackOffice Processes - DigiPalengke
  description: API Endpoints for backoffice processes intended for supermarket operators.
  version: "1.0.0"
paths:
    /createstore:
        get:
          operationId: createstore
          description: Create new store and superadmin user of the store.
          parameters:
            - in: query
              name: storename
              description: The name of the store.
              required: true
              schema:
                type: string
            - in: query
              name: mobile
              description: Representative s mobile number. Backend to auto convert number into +6309 format.
              required: true
              schema:
                type: string
                format: 09 or 63
            - in: query
              name: email
              description: Representative s email address.
              required: true
              schema:
                type: string
            - in: query
              name: fullname
              description: Full name of the admin.
              schema:
                type: string
            - in: query
              name: location
              description: City of store.
              schema:
                type: string
            - in: query
              name: username
              description: The set username of the super admin of the store. 
              required: true
              schema:
                type: string
            - in: query
              name: password
              description: Special MD5 Talino encrypted password submitted by the user.
              schema:
                type: string
                format: Talino encrypted.
            - in: query
              name: key
              description: Talino secured key that is generated using adminusername and mobile.
              schema:
                type: string
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        result:
                          type: string
                          example: success | failed
                        message:
                          type: string
                          example: You have successfully created a new store.
            '400':
              description: failed
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        message:
                          type: string
                          example: You have provided an invalid parameter/s.
    /createuser:
        get:
          operationId: createuser
          description: Create new user to manage the store and process orders.
          parameters:
            - in: query
              name: username
              description: The set unique username.
              required: true
              schema:
                type: string
            - in: query
              name: password
              description: Special MD5 Talino encrypted password submitted by the user.
              schema:
                type: string
                format: Talino encrypted.
            - in: query
              name: storekey
              description: The store in which the user is associated to.
              schema:
                type: string
            - in: query
              name: creatorkey
              required: true
              description: Unique key/id of the user that creates the user account. 
              schema:
                type: string
            - in: query
              name: key
              description: Talino secured key that is generated using username and creatorkey.
              schema:
                type: string
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        result:
                          type: string
                          example: success | failed
                        message:
                          type: string
                          example: You have successfully created a new user.
            '400':
              description: failed
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        message:
                          type: string
                          example: You have provided an invalid parameter/s.
    /login:
        get:
          operationId: loginuser
          description: Process login intent from the user by validating username and password. (Set server cache to limit login attempt to 1 attempt per 2 seconds.)
          parameters:
            - in: query
              name: username
              description: The set unique username.
              required: true
              schema:
                type: string
            - in: query
              name: password
              description: Special MD5 Talino encrypted password submitted by the user.
              schema:
                type: string
                format: Talino encrypted.
            - in: query
              name: key
              description: Talino secured key that is generated using username and encrypted password.
              schema:
                type: string
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        result:
                          type: string
                          example: success | failed
                        message:
                          type: string
                          example: Login successful.
                        storekey:
                          type: string
                        loginkey:
                          type: string
            '400':
              description: failed
              content:
                application/json:
                  schema:
                      type: string
                      properties:
                        message:
                          type: string
                          example: You have provided an invalid parameter/s.
    /getorderlist:
        get:
          operationId: getorderlist
          description: Get the list of orders on a given date and store.
          parameters:
            - in: query
              name: storekey
              description: The unique store ID/key in which the order has been placed. (should not be auto increment ID. Must be alphanumeric.)
              required: true
              schema:
                type: string
            - in: query
              name: userid
              description: User ID/Key of the user accesssing the list. The user must be associated to the store being accessed.
              required: true
              schema:
                type: string
            - in: query
              name: filter
              description: Select the type of filter for the order list. Options are delivery_date or purchase_date.
              schema:
                type: string
                example: deliverydate.
            - in: query
              name: date
              description: Date to apply for the selected filter type. MM-DD-YYYY
              schema:
                type: string
                format: MM-DD-YYYY
            - in: query
              name: key
              description: Talino secured key that is generated using storekey, userid, and date.
              schema:
                type: string
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        result:
                          type: string
                          enum:
                          - "Failed"
                          - "Success"
                        message:
                          type: string
                          example: You have provided an invalid parameters.
                        count:
                          type: integer
                          description: The total number of order made on the selected store and date.
                          example: 20
                        ordervalue:
                          type: number
                          description: The total sales value of all orders and fees due on selected store and date.
                          example: 20000.00
                        ordersummary:
                          type: object
                          description: A JSON format list of basic details about the order which includes the following order ID, orderno, datetimecreated, customer name, contact no, full address, payment type, delivery type, total amount due.
            '400':
              description: failed
              content:
                application/json:
                  schema:
                      type: string
                      properties:
                        message:
                          type: string
                          example: You have provided an invalid parameter/s.
    /getorderdetails:
        get:
          operationId: getorderdetails
          description: Get detailed list of order content from the given order number.
          parameters:
            - in: query
              name: storekey
              description: The unique store ID/key in which the order has been placed. (should not be auto increment ID. Must be alphanumeric.)
              required: true
              schema:
                type: string
            - in: query
              name: orderno
              description: The unique order number.
              schema:
                type: string
            - in: query
              name: key
              description: Talino secured key that is generated using order number, and storekey.
              schema:
                type: string
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                      type: object
                      properties:
                        result:
                          type: string
                          enum:
                          - "Failed"
                          - "Success"
                        message:
                          type: string
                          example: We have found the order details.
                        uniquecount:
                          type: integer
                          description: The total number of unique items in the order.
                          example: 20
                        subtotal:
                          type: number
                          description: The cost of goods.
                          example: 2000.00
                        deliveryfee:
                          type: number
                          description: The due amount for the delivery.
                          example: 50.00
                        paymentfee:
                          type: number
                          description: The due amount for processing payment.
                          example: 0.00
                        otherfees:
                          type: number
                          description: The added fees per order.
                          example: 0.00
                        tax:
                          type: number
                          description: The due amount for taxes.
                          example: 0.00
                        totalprice:
                          type: number
                          description: The grand total which includes subtotal, delivery fee, payment fee, other fees, and taxes.
                          example: 0.00
                        orderlist:
                          type: object
                          description: The JSON format list of all the products, prices, qty added to the cart. Values -  id, productname, unitprice, price, qty
            '400':
              description: failed
              content:
                application/json:
                  schema:
                      type: string
                      properties:
                        message:
                          type: string
                          example: You have provided an invalid parameter/s.