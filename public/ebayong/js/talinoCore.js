//var $globalServer = "https://digipalengke.df.r.appspot.com";
//var $globalServer = "http://localhost/public/digipalengke/api";
var $globalServer = "https://digipalengke.herokuapp.com/api/v1";


//Locations
const brgyCity = [
    {city: "Balanga City", coveredBarangays:["Munting Batangas","Tenejero","Camacho","Bagong Silang","Bagumbayan"]},
    {city: "Dinalupihan", coveredBarangays:["Padre Dandan","Bonifacio","Dalao","Happy Valley"]}
    ];

//********* END: DEFAULT VALUES *************//

function addComma(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
function limitShop(){
    
    var HttpURL = $globalServer + "/countOrder";
        
        request = new XMLHttpRequest();
        request.open("GET", HttpURL ,false);
        request.send();
                
        var data = JSON.parse(request.responseText);
        var $response = data.response;
        var $orderCount = data.orderCount;
            $orderCount = parseInt($orderCount);
    
        //Note: this is a static limit of 30 transactions
        if($orderCount > 30){
            alert("ORDERING CLOSED: Sorry, dahil sa mataas na bilang ng order, na-reach na ng DigiPalengke ang daily order limit nito para sa susunod na delivery. Kami ay muling tatanggap ng order sa ganap na 3:01PM.");
            localStorage.setItem('TotalPrice', 0);
            window.location.replace("index.html?ref=daily+order+limit");
        }else{
            //console.log("Count still good at " + $orderCount);
        }
    
    
    
}
function loadbarangay(){
    var selectedCity = localStorage.getItem('selectedCity'); 
        for(n=0; n < brgyCity.length; n++){
            
            var arrayCity = brgyCity[n]['city'];
            
            if(arrayCity == selectedCity){
                var coveredBarangay = brgyCity[n]['coveredBarangays'];

                var optionBuild = "<option disabled selected>Select Barangay</option>";
                for(n=0; n < coveredBarangay.length; n++){
                    optionBuild += "<option>"+coveredBarangay[n]+"</option>";
                }
                document.getElementById('barangay').innerHTML = optionBuild;

            }
        }
}

//Security
function priceEncrypt(price, totalQty){
    
    var code = price + totalQty + 12345620932397890;
    var hash = md5(code);
    localStorage.setItem('priceCrypt', hash);
}
function priceDecrypt(){
    var price = localStorage.getItem('TotalPrice'); 
        price = parseFloat(price);
    var totalQty = localStorage.getItem('totalQty'); 
        totalQty = parseFloat(totalQty);
    var expectedValue = localStorage.getItem('priceCrypt'); 
    
    var code = price + totalQty + 12345620932397890;
    var hash = md5(code);
    
    console.log(hash + " | " + expectedValue);
    
    if(hash != expectedValue){
        
        
        alert('We have detected an unusual activity from your device. We are will collect necessary information from your transaction for further investigation.');
        window.location.replace("https://www.lawphil.net/statutes/repacts/ra2012/ra_10175_2012.html");
        throw new Error("my error message");
    }else{
        console.log('Price Match');
    }
    
}

function apiKeygen(keywords){
    var salt = "SXsfyOwfA1";
    var key =  keywords + salt;
    var returnVal = md5(key);
    
    return returnVal;
}

function visitFacebook(){
    window.location.replace("https://fb.com/digipalengke");
}

function sendFeedback(){
    window.location.replace("https://fb.com/digipalengke");
}
